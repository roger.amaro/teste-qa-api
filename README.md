# teste-QA

## Começando

Para executar o projeto, será necessário instalar os seguintes programas:

- [Python 3.8](https://www.python.org/downloads/)

- [Pipenv - Gerenciador de dependências](https://pypi.org/project/pipenv/)

### Execução

Para iniciar os script de teste precisamos primeiro instalar as dependências do projeto:

```shell
pipenv install
```

O comando irá instalar as dependências do projeto e criará um virtual environment para 
a execução do projeto, após a instalacão vamos executar o comando behave para iniciar 
os testes

```shell
pytest -vv
```
