import requests as rq

base_url = "http://5d9cc58566d00400145c9ed4.mockapi.io/shopping_cart"
post_json = {'shopping_cart': 1,
             'sku': ['demo_2', 'demo_1', 'demo_7'],
             'color': ['Black', 'Orange', 'Yellow'],
             'size': ['S', 'S', 'S'],
             'price': ['27.00', '16.51', '16.40'],
             'total_shipping': '2.00'}


def test_get_api_response():
    response = rq.get(base_url)
    assert response.status_code == 200


def test_cria_order():
    post_json.update({"color": ["Green", "Red", "Purple"]})
    response = rq.post(base_url, post_json)
    assert response.status_code == 201
    id_user = response.json().get("id")
    response = rq.get(base_url + f"/{id_user}")
    assert response.status_code == 200
    assert response.json().get("color") == ["Green", "Red", "Purple"]
    response = rq.delete(base_url + f"/{id_user}")
    assert response.status_code == 200


def test_update_order():
    post_json.update({"color": ["Green", "Red", "Purple"]})
    response = rq.post(base_url, post_json)
    assert response.status_code == 201
    id_user = response.json().get("id")
    response = rq.get(base_url + f"/{id_user}")
    assert response.status_code == 200, response.json()
    assert response.json().get("color") == ["Green", "Red", "Purple"], response.json()

    json_update = response.json()
    json_update.update(dict(size=["G", "G", "G"]))
    response = rq.put(base_url, json_update)
    assert response.status_code == 200, response.json()
    response = rq.get(base_url + f"/{id_user}")
    assert response.json().get("size") == ["G", "G", "G"], response.json()


def test_delete_order():
    response = rq.post(base_url, post_json)
    assert response.status_code == 201
    id_user = response.json().get("id")
    response = rq.get(base_url + f"/{id_user}")
    assert response.status_code == 200, response.json()

    response = rq.delete(base_url + f"/{id_user}")
    assert response.status_code == 200

    response = rq.get(base_url + f"/{id_user}")
    assert response.status_code == 404

